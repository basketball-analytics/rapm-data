## Historical RAPM Data

- 2018-19
    - [Regular Season](season/2018-19/regular-season)
    - [Playoffs](season/2018-19/playoffs)
- 2017-18
    - [Regular Season](season/2017-18/regular-season)
    - [Playoffs](season/2017-18/playoffs)
- 2016-17
    - [Regular Season](season/2016-17/regular-season)
    - [Playoffs](season/2016-17/playoffs)
- 2015-16
    - [Regular Season](season/2015-16/regular-season)
    - [Playoffs](season/2015-16/playoffs)
- 2014-15
    - [Regular Season](season/2014-15/regular-season)
    - [Playoffs](season/2014-15/playoffs)
- 2013-14
    - [Regular Season](season/2013-14/regular-season)
    - [Playoffs](season/2013-14/playoffs)
- 2012-13
    - [Regular Season](season/2012-13/regular-season)
    - [Playoffs](season/2012-13/playoffs)
- 2011-12
    - [Regular Season](season/2011-12/regular-season)
    - [Playoffs](season/2011-12/playoffs)
- 2010-11
    - [Regular Season](season/2010-11/regular-season)
    - [Playoffs](season/2010-11/playoffs)
- 2009-10
    - [Regular Season](season/2009-10/regular-season)
    - [Playoffs](season/2009-10/playoffs)
- 2008-09
    - [Regular Season](season/2008-09/regular-season)
    - [Playoffs](season/2008-09/playoffs)
- 2007-08
    - [Regular Season](season/2007-08/regular-season)
    - [Playoffs](season/2007-08/playoffs)
- 2006-07
    - [Regular Season](season/2006-07/regular-season)
    - [Playoffs](season/2006-07/playoffs)
- 2005-06
    - [Regular Season](season/2005-06/regular-season)
    - [Playoffs](season/2005-06/playoffs)
- 2004-05
    - [Regular Season](season/2004-05/regular-season)
    - [Playoffs](season/2004-05/playoffs)
- 2003-04
    - [Regular Season](season/2003-04/regular-season)
    - [Playoffs](season/2003-04/playoffs)
- 2002-03
    - [Regular Season](season/2002-03/regular-season)
    - [Playoffs](season/2002-03/playoffs)
- 2001-02
    - [Regular Season](season/2001-02/regular-season)
    - [Playoffs](season/2001-02/playoffs)
- 2000-01
    - [Regular Season](season/2000-01/regular-season)
    - [Playoffs](season/2000-01/playoffs)
- 1999-00
    - [Regular Season](season/1999-00/regular-season)
    - [Playoffs](season/1999-00/playoffs)
- 1998-99
    - [Regular Season](season/1998-99/regular-season)
    - [Playoffs](season/1998-99/playoffs)
- 1997-98
    - [Regular Season](season/1997-98/regular-season)
    - [Playoffs](season/1997-98/playoffs)
- 1996-97
    - [Regular Season](season/1996-97/regular-season)
    - [Playoffs](season/1996-97/playoffs)
