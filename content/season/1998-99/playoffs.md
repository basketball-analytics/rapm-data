---
title: 1998-99 Playoffs RAPM
comments: false
date: 1999-07-01
---

<table id="rapm">
        <thead>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </tfoot>
    </table>

<script>
$(document).ready(function() {
    $('#rapm').DataTable( {
        "ajax": '../../../data/1998-99-playoffs-rapm.json',
        "pageLength": 25
    } );
} );
</script>