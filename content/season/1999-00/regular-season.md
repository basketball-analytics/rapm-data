---
title: 1999-00 Regular Season RAPM
comments: false
date: 2000-04-01
---

<table id="rapm">
        <thead>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Rank</th>
                <th>Player</th>
                <th>Team</th>
                <th>Poss</th>
                <th>ORAPM</th>
                <th>DRAPM</DR>
                <th>RAPM</DR>
            </tr>
        </tfoot>
    </table>

<script>
$(document).ready(function() {
    $('#rapm').DataTable( {
        "ajax": '../../../data/1999-00-rapm.json',
        "pageLength": 25
    } );
} );
</script>