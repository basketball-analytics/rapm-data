---
title: About 
subtitle: Background on RAPM calculations and data
comments: false
---

For a detailed background on the motivation and theory behind how these numbers were calculated, refer to this [paper](../../open-source-data-nba.pdf).

For an overview of the code and a tutorial for calculating these numbers, refer to this [notebook](https://gitlab.com/basketball-analytics/rapm-model/blob/700a3bf72237e6b1bc99e253cbaef5dadd272a9c/rapm_pipeline.ipynb).